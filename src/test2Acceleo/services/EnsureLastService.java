package test2Acceleo.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.OpaqueExpression;

public class EnsureLastService {
	public Collection<ActivityEdge> ensureElseLast(Collection<ActivityEdge> cae, String last) {
		List<ActivityEdge> sorted = new ArrayList<>();
		ActivityEdge lastEdge = null;
		for(ActivityEdge x : cae)
		{
			List<String> body = ((OpaqueExpression)x.getGuard()).getBodies();
			if(!body.isEmpty())
			{
				if(body.get(0).equals(last))
				{
					lastEdge = x;
				} 
				else 
				{
					sorted.add(x);
				}
			} 
			else
			{
				lastEdge = x;
			}

		}
		if(lastEdge != null)
		{
			sorted.add(lastEdge);
		}
		return sorted;
	}
}
